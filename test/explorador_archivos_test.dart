import 'package:ajuste_tempo_app/archivo.dart';
import 'package:ajuste_tempo_app/explorador_archivos.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'explorador_archivos_test.mocks.dart';

Stream<List<int>> contenidoComoFlujo(List<int> contenido) async* {
  for (var byte in contenido) {
    yield [byte];
  }
}

@GenerateMocks([FilePicker])
void main() {
  test('Si no se elige un archivo, no se devuelve nada', () async {
    final interfazExplorador = MockFilePicker();
    final explorador = ExploradorArchivos(interfazExplorador);

    when(interfazExplorador.clearTemporaryFiles())
        .thenAnswer((_) async => true);
    when(interfazExplorador.pickFiles(
            type: anyNamed('type'),
            withReadStream: anyNamed('withReadStream'),
            allowedExtensions: anyNamed('allowedExtensions')))
        .thenAnswer((_) => null);

    Archivo resultado = await explorador.abrir();

    expect(resultado, isNull);
  });

  test('Si se elige un archivo, se guarda su nombre y su contenido', () async {
    final interfazExplorador = MockFilePicker();
    final explorador = ExploradorArchivos(interfazExplorador);
    final nombreArchivo = 'prueba.txt';
    final contenidoArchivo = [100, 50, 20];

    when(interfazExplorador.clearTemporaryFiles())
        .thenAnswer((_) async => true);
    when(interfazExplorador.pickFiles(
            type: anyNamed('type'),
            withReadStream: anyNamed('withReadStream'),
            allowedExtensions: anyNamed('allowedExtensions')))
        .thenAnswer((_) async => FilePickerResult([
              PlatformFile(
                  name: nombreArchivo,
                  readStream: contenidoComoFlujo(contenidoArchivo))
            ]));

    Archivo resultado = await explorador.abrir();

    expect(resultado.nombre, equals(nombreArchivo));
    expect(resultado.contenido, equals(contenidoArchivo));
  });
}
