import 'package:mockito/mockito.dart' as _i1;
import 'package:file_picker/src/file_picker.dart' as _i2;

/// A class which mocks [FilePicker].
///
/// See the documentation for Mockito's code generation for more information.
class MockFilePicker extends _i1.Mock implements _i2.FilePicker {
  MockFilePicker() {
    _i1.throwOnMissingStub(this);
  }
}
