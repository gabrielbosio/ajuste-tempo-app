import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:ajuste_tempo_app/conversor_audio.dart';
import 'package:flutter_test/flutter_test.dart';

Uint8List int16ComoBytes(int numero) =>
    Uint8List(2)..buffer.asByteData().setInt16(0, numero, Endian.little);

Uint8List int32ComoBytes(int numero) =>
    Uint8List(4)..buffer.asByteData().setInt32(0, numero, Endian.little);

List<int> otrasPartes(int cantidad) {
  final codec = Utf8Codec();
  final otraParte = [
    ...codec.encode('otro'),
    ...int32ComoBytes(32),
    ...List<int>.generate(32, (_) => Random().nextInt(256))
  ];

  return List<int>.generate(otraParte.length * cantidad,
      (indice) => otraParte[indice % otraParte.length]);
}

Uint8List datosComoWav(Uint8List muestras,
    {int codigoFormato,
    int cantidadCanales,
    int frecuenciaMuestreo,
    int profundidad,
    bool conParteFormato = true,
    bool conPartesPosterioresADatos = false}) {
  final codec = Utf8Codec();
  final parteFormato = [
    ...int16ComoBytes(codigoFormato),
    ...int16ComoBytes(cantidadCanales),
    ...int32ComoBytes(frecuenciaMuestreo),
    ...List<int>.filled(6, 0),
    ...int16ComoBytes(profundidad)
  ];
  List<int> parteWave = [
    ...codec.encode(conParteFormato ? 'fmt ' : 'otro'),
    ...int32ComoBytes(parteFormato.length),
    ...parteFormato,
    ...otrasPartes(1),
    ...codec.encode('data'),
    ...int32ComoBytes(muestras.length),
    ...muestras,
    ...(conPartesPosterioresADatos ? otrasPartes(4) : [])
  ];
  final audio = [
    ...codec.encode('RIFF'),
    ...int32ComoBytes(parteWave.length + 4),
    ...codec.encode('WAVE'),
    ...parteWave
  ];

  return Uint8List.fromList(audio);
}

void main() {
  const formatoPCM = 1;
  const formatoPuntoFlotante = 3;

  test('Conversor remueve partes del audio anteriores a la de datos', () {
    final parteDatos = Uint8List.fromList([100, 20, 50]);
    Uint8List audio = datosComoWav(parteDatos,
        codigoFormato: formatoPCM,
        cantidadCanales: 1,
        frecuenciaMuestreo: 16000,
        profundidad: 8);
    final conversor = ConversorAudio();

    final audioSoloConDatos = conversor.ejecutar(audio);

    expect(audioSoloConDatos.length, isNot(equals(audio.length)));
    expect(audioSoloConDatos, equals([100, 20, 50]));
  });

  test('Conversor remueve partes del audio posteriores a la de datos', () {
    final parteDatos = Uint8List.fromList([100, 20, 50]);
    Uint8List audio = datosComoWav(parteDatos,
        codigoFormato: formatoPCM,
        cantidadCanales: 1,
        frecuenciaMuestreo: 16000,
        profundidad: 8,
        conPartesPosterioresADatos: true);
    final conversor = ConversorAudio();

    final audioSoloConDatos = conversor.ejecutar(audio);

    expect(audioSoloConDatos.length, isNot(equals(audio.length)));
    expect(audioSoloConDatos, equals([100, 20, 50]));
  });

  test('Conversor solo acepta audio con frecuencia de muestreo de 16 KHz', () {
    final parteDatos = Uint8List.fromList([100, 20, 50]);
    Uint8List audio = datosComoWav(parteDatos,
        codigoFormato: formatoPCM,
        cantidadCanales: 1,
        frecuenciaMuestreo: 44100,
        profundidad: 8);
    final conversor = ConversorAudio();

    final audioSoloConDatos = conversor.ejecutar(audio);

    expect(audioSoloConDatos, isNull);
  });

  test('Conversor solo acepta audio con profundidad de 8 bits', () {
    final parteDatos = Uint8List.fromList([100, 20, 50]);
    Uint8List audio = datosComoWav(parteDatos,
        codigoFormato: formatoPCM,
        cantidadCanales: 1,
        frecuenciaMuestreo: 16000,
        profundidad: 16);
    final conversor = ConversorAudio();

    final audioSoloConDatos = conversor.ejecutar(audio);

    expect(audioSoloConDatos, isNull);
  });

  test('Conversor solo acepta audio en mono', () {
    final parteDatos = Uint8List.fromList([100, 20, 50]);
    Uint8List audio = datosComoWav(parteDatos,
        codigoFormato: formatoPCM,
        cantidadCanales: 2,
        frecuenciaMuestreo: 16000,
        profundidad: 8);
    final conversor = ConversorAudio();

    final audioSoloConDatos = conversor.ejecutar(audio);

    expect(audioSoloConDatos, isNull);
  });

  test('Conversor solo acepta audio con formato PCM', () {
    final parteDatos = Uint8List.fromList([100, 20, 50]);
    Uint8List audio = datosComoWav(parteDatos,
        codigoFormato: formatoPuntoFlotante,
        cantidadCanales: 1,
        frecuenciaMuestreo: 16000,
        profundidad: 8);
    final conversor = ConversorAudio();

    final audioSoloConDatos = conversor.ejecutar(audio);

    expect(audioSoloConDatos, isNull);
  });

  test('Conversor no acepta audio sin parte de formato', () {
    final parteDatos = Uint8List.fromList([100, 20, 50]);
    Uint8List audio = datosComoWav(parteDatos,
        codigoFormato: formatoPCM,
        cantidadCanales: 1,
        frecuenciaMuestreo: 16000,
        profundidad: 8,
        conParteFormato: false);
    final conversor = ConversorAudio();

    final audioSoloConDatos = conversor.ejecutar(audio);

    expect(audioSoloConDatos, isNull);
  });
}
