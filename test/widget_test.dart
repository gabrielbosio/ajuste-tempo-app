import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:ajuste_tempo_app/main.dart';

void main() {
  testWidgets('Smoke test', (WidgetTester tester) async {
    await tester.pumpWidget(ConfiguracionAjustador());

    expect(find.widgetWithText(ElevatedButton, 'Abrir'), findsOneWidget);
    expect(find.widgetWithText(ElevatedButton, 'Enviar al ajustador'),
        findsOneWidget);
  });
}
