import 'package:ajuste_tempo_app/empaquetador.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Empaquetar pista, umbral y tempo de tres cifras', () {
    final empaquetador = Empaquetador();
    final pistaAudio = [100, 20, 50];
    final tempo = 123;
    final umbralMinimo = 20;
    final umbralPolirritmico = false;

    List<int> paquete = empaquetador.empaquetar(
        pistaAudio, tempo, umbralMinimo, umbralPolirritmico);

    expect(paquete, equals([1, 2, 3, 20, 0, 100, 20, 50]));
  });

  test('Empaquetar pista, umbral y tempo de una cifra', () {
    final empaquetador = Empaquetador();
    final pistaAudio = [100, 20, 50];
    final tempo = 8;
    final umbralMinimo = 5;
    final umbralPolirritmico = false;

    List<int> paquete = empaquetador.empaquetar(
        pistaAudio, tempo, umbralMinimo, umbralPolirritmico);

    expect(paquete, equals([0, 0, 8, 5, 0, 100, 20, 50]));
  });

  test('Empaquetar pista, tempo y umbral polirrítmico', () {
    final empaquetador = Empaquetador();
    final pistaAudio = [100, 20, 50];
    final tempo = 8;
    final umbralMinimo = 5;
    final umbralPolirritmico = true;

    List<int> paquete = empaquetador.empaquetar(
        pistaAudio, tempo, umbralMinimo, umbralPolirritmico);

    expect(paquete, equals([0, 0, 8, 5, 1, 100, 20, 50]));
  });
}
