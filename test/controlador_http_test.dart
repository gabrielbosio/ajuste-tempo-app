import 'package:ajuste_tempo_app/controlador_http.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';
import 'package:mockito/annotations.dart';
import 'controlador_http_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  test('Si el envío es exitoso, devuelve la respuesta del request', () async {
    final controlador = ControladorHttp();
    final cliente = MockClient();
    final contenido = [100, 50, 20];
    final codigoEstadoExitoso = 200;
    final respuestaExitosa = '200 OK';

    when(cliente.post(any, headers: anyNamed('headers'), body: contenido))
        .thenAnswer(
            (_) async => http.Response(respuestaExitosa, codigoEstadoExitoso));

    String respuestaActual = await controlador.enviar(contenido, cliente);

    expect(respuestaActual, equals(respuestaExitosa));
  });

  test('Si el envío no es exitoso, no devuelve la respuesta del request',
      () async {
    final controlador = ControladorHttp();
    final cliente = MockClient();
    final contenido = [100, 50, 20];
    final codigoEstadoFallido = 404;
    final respuestaFallida = '404 Not Found';

    when(cliente.post(any, headers: anyNamed('headers'), body: contenido))
        .thenAnswer(
            (_) async => http.Response(respuestaFallida, codigoEstadoFallido));

    String respuestaActual = await controlador.enviar(contenido, cliente);

    expect(respuestaActual, isNot(equals(respuestaFallida)));
  });
}
