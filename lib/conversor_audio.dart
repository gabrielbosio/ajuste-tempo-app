import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

/// Conversor de audio WAV PCM mono de 8 bits y 16 KHz.
class ConversorAudio {
  static const _codigoFormato = 1;
  static const _cantidadCanales = 1;
  static const _frecuenciaMuestreo = 16000;
  static const _profundidad = 8;
  final _codec = Utf8Codec();

  /// Convierte muestras de audio WAV a muestras sin formato.
  ///
  /// Un archivo WAV consiste en un encabezado y un cuerpo. El cuerpo tiene dos
  /// partes principales: `fmt` y `data`.
  /// [audio] debe ser una secuencia de bytes de un archivo WAV PCM mono de 8
  /// bits y 16 KHz. Si [audio] cumple con estos requisitos y contiene las
  /// partes principales de un archivo WAV, este metodo retorna la parte `data`
  /// de [audio]. De lo contrario, retorna `null`.
  Uint8List ejecutar(Uint8List audio) {
    Iterable<int> partesWave = audio.skip(12);
    String idParteFormato = _idParte(partesWave);
    if (idParteFormato != 'fmt ') {
      return null;
    }
    Iterable<int> parteYLongitudFormato = partesWave.skip(4);
    int longitudParteFormato =
        _primerosBytesComoEntero(parteYLongitudFormato, 4);
    Iterable<int> parteFormato = parteYLongitudFormato.skip(4);
    if (!_cumpleConRequisitos(parteFormato)) {
      return null;
    }
    List<int> otrasPartes = parteFormato.skip(longitudParteFormato).toList();
    List<int> parteYLongitudDatos = _saltarAParteDatos(otrasPartes);
    int longitudParteDatos = _primerosBytesComoEntero(parteYLongitudDatos, 4);
    List<int> parteDatos = parteYLongitudDatos.skip(4).toList();
    Iterable<int> muestrasAudio = parteDatos.getRange(0, longitudParteDatos);

    return Uint8List.fromList(muestrasAudio.toList());
  }

  List<int> _saltarAParteDatos(List<int> partes) {
    while (partes.isNotEmpty && _idParte(partes) != 'data') {
      Iterable<int> bytesParte = partes.skip(4);
      int longitudParte = _primerosBytesComoEntero(bytesParte, 4);
      Iterable<int> parte = bytesParte.skip(4);
      partes = parte.skip(longitudParte).toList();
    }

    return partes.skip(4).toList();
  }

  int _primerosBytesComoEntero(Iterable<int> bytes, int longitud) {
    List<int> primerosBytes = bytes.toList().getRange(0, longitud).toList();
    int resultado = 0;
    for (var i = 0; i < primerosBytes.length; i++) {
      resultado += primerosBytes[i] * pow(256, i);
    }

    return resultado;
  }

  String _idParte(Iterable<int> parte) =>
      _codec.decode(parte.toList().getRange(0, 4).toList());

  bool _cumpleConRequisitos(Iterable<int> parteFormato) {
    int codigoFormato = _primerosBytesComoEntero(parteFormato, 2);
    int cantidadCanales = _primerosBytesComoEntero(parteFormato.skip(2), 2);
    int frecuenciaMuestreo = _primerosBytesComoEntero(parteFormato.skip(4), 4);
    int profundidad = _primerosBytesComoEntero(parteFormato.skip(14), 2);

    return codigoFormato == _codigoFormato &&
        cantidadCanales == _cantidadCanales &&
        frecuenciaMuestreo == _frecuenciaMuestreo &&
        profundidad == _profundidad;
  }
}
