import 'dart:async';
import 'dart:typed_data';

import 'package:ajuste_tempo_app/archivo.dart';
import 'package:ajuste_tempo_app/controlador_http.dart';
import 'package:ajuste_tempo_app/conversor_audio.dart';
import 'package:ajuste_tempo_app/empaquetador.dart';
import 'package:ajuste_tempo_app/explorador_archivos.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(ConfiguracionAjustador());
}

class ConfiguracionAjustador extends StatefulWidget {
  ConfiguracionAjustador({Key key}) : super(key: key);

  @override
  _EstadoConfiguracionAjustador createState() {
    return _EstadoConfiguracionAjustador();
  }
}

class _EstadoConfiguracionAjustador extends State<ConfiguracionAjustador> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Configuración del ajustador',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Contenido(),
    );
  }
}

class Contenido extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => EstadoContenido();
}

class EstadoContenido extends State<Contenido> {
  Future<String> _respuestaEnvio;
  String _nombrePista;
  List<int> _contenidoPista;
  int _tempo;
  var _umbralMinimo = 20;
  var _umbralPolirritmico = false;
  final _controladorHttp = ControladorHttp();
  final _exploradorArchivos = ExploradorArchivos(FilePicker.platform);
  final _empaquetador = Empaquetador();
  final _conversor = ConversorAudio();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Configuración del ajustador'),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(8.0),
        child: (_respuestaEnvio == null) ? _armarInicio() : _armarReceptor(),
      ),
    );
  }

  Column _armarInicio() {
    var filaArchivo = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(
            child: TextField(
                readOnly: true,
                decoration: InputDecoration(
                    hintText: _nombrePista == null
                        ? '<Cargue un archivo>'
                        : _nombrePista))),
        ElevatedButton(onPressed: _abrirExplorador, child: Text('Abrir'))
      ],
    );

    var filaTempo =
        Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Text('Tempo: '),
      Container(
          width: 50,
          child: TextField(
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              maxLength: 3,
              onSubmitted: (nuevoTempo) => setState(() {
                    _tempo = int.tryParse(nuevoTempo);
                  }))),
      Text(' bpm'),
    ]);

    var filaUmbralMinimo = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text('Umbral mínimo: '),
        Container(
            width: 50,
            child: TextFormField(
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                maxLength: 2,
                initialValue: _umbralMinimo.toString(),
                onFieldSubmitted: (nuevoUmbral) => setState(() {
                      _umbralMinimo = int.tryParse(nuevoUmbral);
                    }))),
        Text(' ms'),
      ],
    );

    var filaUmbralPolirritmico = CheckboxListTile(
      title: const Text('Permitir tresillos'),
      value: _umbralPolirritmico,
      secondary: const Icon(Icons.music_note),
      onChanged: (nuevoUmbral) => setState(() {
        _umbralPolirritmico = nuevoUmbral;
      }),
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        filaArchivo,
        filaTempo,
        filaUmbralMinimo,
        filaUmbralPolirritmico,
        ElevatedButton(
          onPressed: _sePuedeEnviar() ? _enviarArchivo : null,
          child: Text('Enviar al ajustador'),
          style: _sePuedeEnviar()
              ? null
              : ElevatedButton.styleFrom(
                  primary: Color.fromARGB(255, 220, 220, 220)),
        ),
      ],
    );
  }

  FutureBuilder<String> _armarReceptor() {
    return FutureBuilder<String>(
      future: _respuestaEnvio,
      builder: (contexto, resultado) {
        var tuvoExito = resultado.hasData;
        var fallo = resultado.hasError;

        if (tuvoExito || fallo) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                tuvoExito ? resultado.data : 'No recibido :(',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              ElevatedButton(
                  onPressed: () => setState(() => _respuestaEnvio = null),
                  child: Text('Volver'))
            ],
          );
        }

        return CircularProgressIndicator();
      },
    );
  }

  void _enviarArchivo() {
    final clienteHttp = http.Client();
    List<int> paquete = _empaquetador.empaquetar(
        _contenidoPista, _tempo, _umbralMinimo, _umbralPolirritmico);
    setState(() {
      _respuestaEnvio = _controladorHttp.enviar(paquete, clienteHttp);
    });
    clienteHttp.close();
  }

  void _abrirExplorador() async {
    Archivo archivo = await _exploradorArchivos.abrir();
    if (!mounted || archivo == null) return;
    final audio = Uint8List.fromList(archivo.contenido);
    final muestrasAudio = _conversor.ejecutar(audio);
    if (muestrasAudio == null) {
      showDialog<String>(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: const Text('Error al cargar archivo'),
                content: const Text(
                    'El archivo de audio debe ser WAV PCM mono de 16 KHz y 8 '
                    'bits.'),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'OK'),
                    child: const Text('OK'),
                  ),
                ],
              ));
      return;
    }
    setState(() {
      _nombrePista = archivo.nombre;
      _contenidoPista = muestrasAudio;
    });
  }

  bool _sePuedeEnviar() {
    return _nombrePista != null &&
        _contenidoPista != null &&
        _tempo != null &&
        _umbralMinimo != null;
  }
}
