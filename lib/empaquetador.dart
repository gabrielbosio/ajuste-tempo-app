/// Empaquetador de valores de configuracion.
class Empaquetador {
  /// Concatena [pistaAudio], [tempo], [umbralMinimo] y [umbralPolirritmico] en
  /// una trama de datos.
  ///
  /// La trama de datos resultante sigue el formato:
  /// `[t0, t1, t2, u_m, u_p, p0, p1, ..., pk]`,
  /// donde:
  /// [tempo] = `t0 * 100 + t1 * 10 + t2`,
  /// [umbralMinimo] = `u_m`,
  /// [umbralPolirritmico] = `u_p`,
  /// [pistaAudio] = `[p0, p1, ..., pk]`.
  /// Retorna la trama de datos.
  List<int> empaquetar(List<int> pistaAudio, int tempo, int umbralMinimo,
      bool umbralPolirritmico) {
    final digitosTempo = [tempo ~/ 100, tempo ~/ 10 % 10, tempo % 10];
    final byteUmbralPolirritmico = umbralPolirritmico ? 1 : 0;
    return [
      ...digitosTempo,
      umbralMinimo,
      byteUmbralPolirritmico,
      ...pistaAudio
    ];
  }
}
