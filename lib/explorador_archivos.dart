import 'package:ajuste_tempo_app/archivo.dart';
import 'package:file_picker/file_picker.dart';

/// Explorador de archivos WAV.
///
/// Este explorador usa [FilePicker] para acceder al almacenamiento del
/// dispositivo que ejecuta esta aplicacion.
class ExploradorArchivos {
  FilePicker _interfazExplorador;

  ExploradorArchivos(this._interfazExplorador);

  /// Abre una ventana de carga de archivos WAV.
  ///
  /// Si el usuario elige un archivo, este metodo crea un nuevo [Archivo]
  /// con el nombre y el contenido del archivo elegido y lo retorna. Si el
  /// usuario cancela la carga de archivos, este metodo retorna `null`.
  Future<Archivo> abrir() async {
    _interfazExplorador.clearTemporaryFiles();
    FilePickerResult resultado = await _interfazExplorador.pickFiles(
      type: FileType.custom,
      withReadStream: true,
      allowedExtensions: ['wav'],
    );

    if (resultado != null) {
      PlatformFile archivo = resultado.files.first;
      List<int> recibido;
      await for (var bytes in archivo.readStream) {
        if (recibido == null) {
          recibido = List.from(bytes);
        } else {
          recibido.addAll(bytes);
        }
      }
      return Archivo(archivo.name, recibido);
    }

    return null;
  }
}
