class Archivo {
  final String nombre;
  final List<int> contenido;

  Archivo(this.nombre, this.contenido);
}
