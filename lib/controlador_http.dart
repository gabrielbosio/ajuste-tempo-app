import 'package:http/http.dart' as http;

/// Capa de abstraccion para el manejo de requests HTTP.
class ControladorHttp {
  /// Envia [contenido] al procesador de audio.
  ///
  /// Este controlador usa [cliente] para enviar al procesador de audio un
  /// request HTTP con los datos de [contenido]. El request es un metodo POST
  /// `enviar` con [contenido] como cuerpo.
  /// Antes de llamar a este metodo, el dispositivo que ejecuta esta aplicacion
  /// debe estar conectado al punto de acceso del procesador de audio.
  /// Si la respuesta al request es 200, retorna el cuerpo de la respuesta. De
  /// lo contrario, retorna un mensaje de error.
  Future<String> enviar(List<int> contenido, http.Client cliente) async {
    final response = await cliente.post(Uri.http('192.168.4.1', 'enviar'),
        headers: {'Content-Type': 'application/octet-stream'}, body: contenido);

    if (response.statusCode == 200) {
      return response.body;
    } else {
      return 'No recibido :(';
    }
  }
}
