# Ajuste del tempo de una pista de audio en base al tempo de una señal de audio externa

Aplicación de configuración para el [procesador de señales](https://gitlab.com/gabrielbosio/ajuste-tempo).

## Empezando a usar
1. Instalar aplicación en un dispositivo móvil.
1. Conectar [procesador de señales](https://gitlab.com/gabrielbosio/ajuste-tempo).
1. Conectar dispositivo móvil a la red del procesador de señales vía Wi-Fi.
1. Iniciar aplicación.